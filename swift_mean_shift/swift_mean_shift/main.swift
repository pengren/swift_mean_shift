//
//  main.swift
//  simple_mean_shift
//
//  Created by renpeng on 8/28/20.
//  Copyright © 2020 renpeng. All rights reserved.
//

import Foundation
public class simple_mean_shift{
    let bandwidth: Double
    let max_iter: Int
    init(bandwidth: Double, max_iter: Int = 300) {
        self.bandwidth = bandwidth
        self.max_iter = max_iter
    }
    func get_bin_seeds(_ X: [[Double]], _ bin_size: Double, _ min_bin_freq: Int = 1) -> [[Double]] {
        // declare an empty dictionary
        var bin_sizes = Dictionary<[Double], Int>()
        for point in X{
            assert(point.count == 2, "point is not [x, y]")
            let binned_point = [round(point[0] / bin_size), round(point[1] / bin_size)]
            if bin_sizes.keys.contains(binned_point) {
              // contains key
                bin_sizes[binned_point]! += 1
            } else {
              // does not contain key
                bin_sizes[binned_point] = 1
            }
        }
        var bin_seeds = [[Double]]()
        for (point, freq) in bin_sizes {
            if freq >= min_bin_freq{
                assert(point.count == 2, "point is not [x, y]")
                // For each value in bin_seeds, multiple the bin_size
                bin_seeds.append([point[0] * bin_size, point[1] * bin_size])
            }
        }
        
        if bin_seeds.count == X.count{
            print("Binning data failed")
            return X
        }
        return bin_seeds
    }
    
    public func average(_ values: [Double]) -> Double{
        let count = Double(values.count)
        assert(count > 0, "input array cannot be empty")
        return values.reduce(0, +) / count
    }
    
    func x_y_mean(_ points_within: [[Double]]) -> [Double]{
        var x_list = [Double]()
        var y_list = [Double]()
        for single_point in points_within{
            assert(single_point.count == 2, "point is not [x, y]")
            x_list.append(single_point[0])
            y_list.append(single_point[1])
        }
        return [average(x_list), average(y_list)]
    }
    
    func l2_distance(_ p1: [Double], _ p2: [Double])->Double{
        assert(p1.count == 2, "point is not [x, y]")
        assert(p2.count == 2, "point is not [x, y]")
        return sqrt(pow(p1[0] - p2[0], 2) + pow(p1[1] - p2[1], 2))
    }
    
    func raduis_neighbors(_ my_mean: [Double], _ X: [[Double]]) -> ([[Double]], [Int]) {
        // my_mean: center points
        // X: all points list
        var points_within = [[Double]]()
        var cluster_index_list = [Int]()
        
        for (index, single_point) in X.enumerated() {
            if self.l2_distance(my_mean, single_point) < self.bandwidth{
                // 'falls within the bandwidth'
                points_within.append(single_point)
                cluster_index_list.append(index)
            }
        }
        return (points_within, cluster_index_list)
    }
    
    func _mean_shift_single_seed(_ single_point: [Double], _ points: [[Double]]) -> ([Double], Int) {
        assert(single_point.count == 2, "point is not [x, y]")
        let stop_thresh = 1e-3 * self.bandwidth // when mean has converged
        var completed_iterations = 0
        var changed_point = single_point // Make sure we can change the value of single_point
        while true {
            // Find mean of points within bandwidth
            let (points_within, _) = self.raduis_neighbors(changed_point, points)
            if points_within.count == 0{
                break  // Depending on seeding strategy this condition may occur
            }
            let old_single_point = changed_point  // save the old mean
            changed_point = self.x_y_mean(points_within)
            // If converged or at max_iter, adds the cluster
            if (l2_distance(changed_point, old_single_point) < stop_thresh) || (completed_iterations == self.max_iter){
                return (changed_point, points_within.count)
            }
            completed_iterations += 1
        }
        // empty results, will skip this condition
        return ([], -1)
    }
    
    func get_target_cluster_index(_ single_point: [Double], _ cluster_centers: [[Double]]) -> Int {
        var minimum_distance = Double.infinity
        var res_index = -1
        
        for (index, center) in cluster_centers.enumerated() {
            let distance = self.l2_distance(single_point, center)
            if distance < minimum_distance{
                minimum_distance = distance
                res_index = index
            }
        }
        assert(res_index != -1, "Nothing found, unexpected behaviour")
        return res_index
    }
    
    func assign_label(_ points: [[Double]], _ cluster_centers: [[Double]]) -> [Int] {
        // for each point in X, figure out its corresponding cluster
        var label = [Int]()
        for single_point in points{
            let target_cluster_index = self.get_target_cluster_index(single_point, cluster_centers)
            label.append(target_cluster_index)
        }
        return label
    }
    
    func fit_X(_ points: [[Double]]) -> ([[Double]], [Int]) {
        assert(points[0].count == 2, "expects 2d array with feature dim 2")
        // try to shrink the computing time
        let bin_points = self.get_bin_seeds(points, self.bandwidth)
        var center_intensity_dict = Dictionary<[Double], Int>()
        // move points
        for single_point in bin_points{
            let (changed_point, neighbor_count) = self._mean_shift_single_seed(single_point, points)
//            print("changed_point: \(changed_point), neighbor_count: \(neighbor_count)")
            if neighbor_count != -1{
                // value: the number of neighbors of the key point
                center_intensity_dict[changed_point] = neighbor_count
            }
        }
        
//        POST PROCESSING: remove near duplicate points
//        If the distance between two kernels is less than the bandwidth,
//        then we have to remove one because it is a duplicate. Remove the
//        one with fewer points.
        // Results here could be different from the python3 ver.
        // results are unobvious when dictionary values are equal when sort them..
        let sorted_by_intensity = center_intensity_dict.sorted { $0.1 >= $1.1 }
        var sorted_centers = [[Double]]()
        for tup in sorted_by_intensity{
            sorted_centers.append(tup.key)
        }
        var unique = Array(repeating: true, count: sorted_centers.count)
        for (i, center) in sorted_centers.enumerated() {
            // eliminate possible recomputing via extra space
            if unique[i]{
                let (_, neighbor_idxs) = self.raduis_neighbors(center, sorted_centers)
                for idx in neighbor_idxs{
                    unique[idx] = false
                }
                unique[i] = true
            }
        }
        var cluster_centers_ = [[Double]]()
        for (i, _) in sorted_centers.enumerated(){
            if unique[i]{
                cluster_centers_.append(sorted_centers[i])
            }
        }
        let labels_ = self.assign_label(points, cluster_centers_)
        return (cluster_centers_, labels_)
    }
}

func read_in_1d_csv(_ filePath: String) -> [Double]{
    let fullPath = NSString(string: filePath).expandingTildeInPath
    do
    {
        let fileContent = try NSString(contentsOfFile: fullPath, encoding: String.Encoding.utf8.rawValue)
        let res = String(fileContent)
        var fullNameArr = res.components(separatedBy: "\r\n")
        let doubles = fullNameArr.compactMap(Double.init)
        return doubles
    }
    catch
    {
        print(error)
    }
    return []
}

func read_in_2d_csv(_ filePath: String) -> [[Double]]{
    let fullPath = NSString(string: filePath).expandingTildeInPath
    do
    {
        let fileContent = try NSString(contentsOfFile: fullPath, encoding: String.Encoding.utf8.rawValue)
        let res = String(fileContent)
        var fullNameArr = res.components(separatedBy: "\r\n")
        var fin_res = [[Double]]()
        for single_str in fullNameArr{
            if single_str.count > 0{
                let val = single_str.components(separatedBy: ",")
                fin_res.append([Double(val[0])!, Double(val[1])!])
            }
        }
        return fin_res
    }
    catch
    {
        print(error)
    }
    return [[]]
}

let SMS = simple_mean_shift(bandwidth: 5.0)
let base_path = "/Users/renpeng/PycharmProjects/geo_json_py/swift_test/"

for index in 1...204{
    print("index: \(index)")
    let input_X = read_in_2d_csv(base_path + "input/\(index).csv")
    let center = read_in_2d_csv(base_path + "center/\(index).csv")
    let label = read_in_1d_csv(base_path + "label/\(index).csv")
    let (cluster_centers_, labels_) = SMS.fit_X(input_X)
    
    print(cluster_centers_)
    assert(center.count == cluster_centers_.count, "output count not equal")
    
    for index in 0..<center.count{
        print(abs(cluster_centers_[index][0] - center[index][0]))
        print(abs(cluster_centers_[index][1] - center[index][1]))
//        if abs(cluster_centers_[index][0] - center[index][0]) > 0.0001{
//            print(index)
//        }
        assert(abs(cluster_centers_[index][0] - center[index][0]) < 0.1, "x center unequal")
        assert(abs(cluster_centers_[index][1] - center[index][1]) < 0.1, "y center unequal")
    }
    
    for j in 0..<label.count{
        if Int(label[j]) != labels_[j]{
            print(index)
        }
        assert(Int(label[j]) == labels_[j], "label not equal")
    }
}
print("Done!")
